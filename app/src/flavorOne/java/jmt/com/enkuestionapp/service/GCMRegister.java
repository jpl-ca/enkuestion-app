package jmt.com.enkuestionapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;

public class GCMRegister extends Service {
    GoogleCloudMessaging gcm;
    String regId;
    public GCMRegister() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gcm = GoogleCloudMessaging.getInstance(this);
        regId = new JmStore(this).get(S.GCMID);
        System.out.println("regID:"+regId);


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("regID222:"+regId);
                    if (gcm == null)gcm = GoogleCloudMessaging.getInstance(GCMRegister.this);
                    regId = gcm.register(S.GOOGLE.PROJECT_ID);
                    System.out.println("regID333:"+regId);
                }catch (IOException ex){
                    ex.printStackTrace();
                }
                stopSelf();
                stopForeground(true);
            }
        });
        thread.start();
    }
    @Override
    public void onDestroy() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startid){
        return START_STICKY;
    }
}