package jmt.com.enkuestionapp.service;

import android.app.Activity;
import android.os.AsyncTask;
import com.google.gson.Gson;
import org.json.JSONObject;
import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class RegistroCuentaTask extends AsyncTask<JSONObject, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    public RegistroCuentaTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(JSONObject... params) {
        ResponseE Z = new ResponseE();
        try{
            Z = new OkHttp(ctx).makePostRequest("/api/user/register",params[0]);
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            new JmStore(ctx).save(S.USUARIO.USUARIO,response.getData());
            delegate.processFinish(response);
        }
        else delegate.processFinish(response.getMassege());
    }
}