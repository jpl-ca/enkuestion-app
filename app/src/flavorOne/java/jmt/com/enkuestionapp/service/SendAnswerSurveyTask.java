package jmt.com.enkuestionapp.service;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.AnswerE;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class SendAnswerSurveyTask extends AsyncTask<ArrayList<AnswerE>, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    long survey_id;
    public SendAnswerSurveyTask(Activity ctx, CallbackRequest delegate,long survey_id){
        this.ctx = ctx;
        this.delegate = delegate;
        this.survey_id = survey_id;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(ArrayList<AnswerE>... params) {
        ResponseE Z = new ResponseE();
        try{
            String answers =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(params[0]);
            JSONObject jo = new JSONObject();
            JSONObject jobj = new JSONObject();
            jo.put("survey_id",survey_id);
            jo.put("answers",answers);
            String data = jo.toString().replace("\\", "").replace("\"[","[").replace("]\"","]");
            jobj.put("data",data);
//            System.out.println("Listo para enviar");
//            System.out.println(answers);
            System.out.println(data);

            Z = new OkHttp(ctx).makePostRequest("/api/survey/store",jobj);
            System.out.println("-----|>");
            System.out.println(Z.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        delegate.processFinish(response);
    }
}