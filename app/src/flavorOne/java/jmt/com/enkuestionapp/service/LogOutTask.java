package jmt.com.enkuestionapp.service;

import android.app.Activity;
import android.os.AsyncTask;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class LogOutTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    JmStore jmstore;
    public LogOutTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        jmstore = new JmStore(ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try {
            Z = new OkHttp(ctx).makeGetRequest("/api/auth/logout");
        } catch (Exception e) {e.printStackTrace(); }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        System.out.println("S>>>"+response.isSuccess());
        if(response.isSuccess()){
            delegate.processFinish(response);
        }
        else delegate.processFinish(response.getMassege());
    }
}