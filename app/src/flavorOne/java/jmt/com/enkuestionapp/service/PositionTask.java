package jmt.com.enkuestionapp.service;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class PositionTask extends AsyncTask<String, String, ResponseE> {
    Context ctx;
    CallbackRequest delegate;
    public PositionTask(Context ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            jo.put("last_position",params[0]);
            System.out.println("Enviando:"+jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/api/user/update-last-position",jo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        delegate.processFinish(response);
    }
}