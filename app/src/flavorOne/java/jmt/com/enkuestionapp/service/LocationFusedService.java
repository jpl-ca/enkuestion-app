package jmt.com.enkuestionapp.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import java.util.Arrays;
import java.util.Date;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.entity.ResponseE;

//import jmt.irisgps.cobratrack.http_service.task.SendBatchLocationTask;
//import jmt.irisgps.cobratrack.interfaz.CallbackRequest;
//import jmt.irisgps.cobratrack.model.DA.LocationHistoryDA;
//import jmt.irisgps.cobratrack.model.entity.LocationHistoriesE;
//import jmt.irisgps.cobratrack.model.entity.ResponseE;
//import jmt.irisgps.cobratrack.util.S;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;
    private long LAST_TIME_UPDATED = 0;
    int totalLocations;
    public final static int TOTAL_PRE_LOC = 3;
    LatLng preLocation[];

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestingLocationUpdates = false;
        intent = new Intent();
        totalLocations = 0;
        preLocation = new LatLng[TOTAL_PRE_LOC];
        Arrays.fill(preLocation, new LatLng(0, 0));
        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        startLocationUpdates();
    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        new PositionTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {}
            @Override
            public void processFinish(ResponseE response) {
                finalizarServicio();
            }
        }).execute(mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude());
    }
    private void finalizarServicio(){
        stopLocationUpdates();
        stopForeground(true);
        stopSelf();
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
}