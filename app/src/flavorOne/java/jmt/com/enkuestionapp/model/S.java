package jmt.com.enkuestionapp.model;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public final class S {
    public static final String TOKEN = "token";
    public static final String GCMID = "gcm";
    public static final String Set_Cookie = "Set-Cookie";
    public static final String Cookie = "Cookie";
    public static final String IMEI = "IMEI";
    public static final class GOOGLE {
        public static final String PROJECT_ID = "537348568021";
    }
    public static final class BROADCAST{
        public static final String NEW_SURVERY = "new_survey";
    }
    public static final class TIPO_ALTERNATIVA {
        public static final int UNICA = 1;
        public static final int MULTIPLE = 2;
        public static final int LIBRE= 3;
    }
    public static final class USUARIO {
        public static final String USUARIO = "usuario";
        public static final String DNI = "dni";
    }
    public static final class TIPO_PREGUNTA {
        public static final int SEL_UNICA = 1;
        public static final int SEL_MULTIPLE = 2;
    }
    public static final class SURVEY {
        public static final String SURVEY_ID = "survey_id";
        public static final String SURVEY_NAME = "survey_name";
        public static final String SURVEY_POINTS= "survey_points";
        public static final String QUESTION_LIST = "question_list";
        public static final String LIST_QUESTION = "list_question";
        public static final int PENDIENTE = 1;
        public static final int ACTIVO = 2;
        public static final int TERMINADO = 3;
        public static final int INACTIVO = 4;
        public static final int CODE_RESULT = 1;
    }
    public static final class RESPONSE {
        public static final String enc_without_connection = "irs000";//error Sin Conexion!
        public static final String enc_success = "irs001";//success	Respuesta para solicitudes exitosas. Toda solicitud realizada de manera correcta deberá de retornar este código
        public static final String enc401 = "irs401";//error	No se tiene Autorización para realizar la solicitud. Token inválido, número no autorizado, número no activado.
        public static final String enc_invalid_data = "irs402";//error	La información enviada no es válida o está incompleta.
        public static final String enc2001 = "irs501";//error	El servidor no pudo procesar la solicitud.
    }
}