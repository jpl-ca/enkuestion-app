package jmt.com.enkuestionapp.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class AlternativeE  implements Serializable {
    @Expose
    @SerializedName("id")
    int alternative_id;
    @Expose
    int alternative_type_id;
    @Expose
    String description;
    @Expose
    int question_id;
    @Expose
    int min_value;
    @Expose
    int max_value;
    @Expose
    String min_tag;
    @Expose
    String max_tag;
    @Expose
    String img_url;
    @Expose
    boolean selected;
    public AlternativeE(int alternative_id,int alternative_type_id,String description,boolean selected){
        this.alternative_id=alternative_id;
        this.alternative_type_id=alternative_type_id;
        this.description=description;
        this.selected=selected;
    }
    public AlternativeE(int alternative_id,int alternative_type_id,String description,int question_id,int min_value,int max_value,String min_tag,String maz_tag,String img_url,boolean selected){
        this.alternative_id=alternative_id;
        this.alternative_type_id=alternative_type_id;
        this.description=description;
        this.question_id=question_id;
        this.min_value=min_value;
        this.max_value=max_value;
        this.min_tag=min_tag;
        this.max_tag=maz_tag;
        this.img_url=img_url;
        this.selected=selected;
    }

    public int getAlternative_id() {
        return alternative_id;
    }

    public void setAlternative_id(int alternative_id) {
        this.alternative_id = alternative_id;
    }

    public int getAlternative_type_id() {
        return alternative_type_id;
    }

    public void setAlternative_type_id(int alternative_type_id) {
        this.alternative_type_id = alternative_type_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getMin_value() {
        return min_value;
    }

    public void setMin_value(int min_value) {
        this.min_value = min_value;
    }

    public int getMax_value() {
        return max_value;
    }

    public void setMax_value(int max_value) {
        this.max_value = max_value;
    }

    public String getMin_tag() {
        return min_tag;
    }

    public void setMin_tag(String min_tag) {
        this.min_tag = min_tag;
    }

    public String getMax_tag() {
        return max_tag;
    }

    public void setMax_tag(String max_tag) {
        this.max_tag = max_tag;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}