package jmt.com.enkuestionapp.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public class SurveyE implements Serializable {
    @SerializedName("id")
    @Expose
    int survey_id;
    @Expose
    String title;
    @SerializedName("reward_points")
    @Expose
    int puntos;
    @Expose
    int survey_state_id;
    @Expose
    String start_date;
    @Expose
    String end_date;
    @SerializedName("img_url")
    @Expose
    String url_image;
    @Expose
    int max_completed_per_survey;

    @Expose
    ArrayList<QuestionE> questions;

    public SurveyE(int survey_id,String title,int puntos,int survey_state_id){
        this.survey_id=survey_id;
        this.title=title;
        this.puntos=puntos;
        this.survey_state_id =survey_state_id;
    }

    public int getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(int survey_id) {
        this.survey_id = survey_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getSurvey_state_id() {
        return survey_state_id;
    }

    public void setSurvey_state_id(int survey_state_id) {
        this.survey_state_id = survey_state_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getMax_completed_per_survey() {
        return max_completed_per_survey;
    }

    public void setMax_completed_per_survey(int max_completed_per_survey) {
        this.max_completed_per_survey = max_completed_per_survey;
    }

    public ArrayList<QuestionE> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionE> questions) {
        this.questions = questions;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }
}