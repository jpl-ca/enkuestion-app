package jmt.com.enkuestionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.onButtonListener;
import jmt.com.enkuestionapp.interfaz.onRegisterListener;
import jmt.com.enkuestionapp.model.entity.UserE;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class DatoUsuarioAdapter extends RecyclerView.Adapter<DatoUsuarioAdapter.ViewHolder> {
    private Context ctx;
    private UserE user;
    public static onButtonListener delegate;
    public DatoUsuarioAdapter(Context _ctx, UserE _user,onButtonListener callback){
        delegate = callback;
        ctx = _ctx;
        user = _user;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_dato_usuario, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delegate.click();
            }
        });
        holder.txt_nombre_usuario.setText(user.getFirst_name());
        holder.txt_apellido_usuario.setText(user.getLast_name());
        holder.txt_fecha_nacimiento.setText(user.getBirth_date().substring(0,4));
        holder.txt_dni.setText(user.getId_doc_number());
        String Sexo = user.getGender()==1?"Masculino":"Femenino";
        holder.txt_genero.setText(Sexo);
        holder.txt_email.setText(user.getEmail());
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        protected Button btn_editar;
        protected TextView txt_nombre_usuario,txt_apellido_usuario,txt_fecha_nacimiento,txt_dni,txt_genero,txt_email;
        protected boolean clickable = false;
        public ViewHolder(View itemView) {
            super(itemView);
            btn_editar = (Button) itemView.findViewById(R.id.btn_editar);
            txt_nombre_usuario = (TextView) itemView.findViewById(R.id.txt_nombre_usuario);
            txt_apellido_usuario = (TextView) itemView.findViewById(R.id.txt_apellido_usuario);
            txt_fecha_nacimiento = (TextView) itemView.findViewById(R.id.txt_fecha_nacimiento);
            txt_dni = (TextView) itemView.findViewById(R.id.txt_dni);
            txt_genero = (TextView) itemView.findViewById(R.id.txt_genero);
            txt_email = (TextView) itemView.findViewById(R.id.txt_email);
        }
    }
}