package jmt.com.enkuestionapp.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.CbOptionSelected;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.AlternativeE;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ViewHolder> {
    private ArrayList<AlternativeE> ds;
    private boolean last_question;
    private Context ctx;
    public static CbOptionSelected delegate;
    AlertDialog levelDialog = null;
    public OptionsAdapter(Context _ctx,boolean _last_question,ArrayList<AlternativeE> dataArgs,CbOptionSelected callback){
        ds = dataArgs;
        delegate = callback;
        ctx = _ctx;
        last_question = _last_question;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_question, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.position = position;
        holder.clickable = position!=ds.size();
        if(holder.clickable){
            final AlternativeE alt = ds.get(position);
            if(alt.isSelected())
                 holder.cv_question.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorPressed));
            else holder.cv_question.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorUnPressed));
            if(alt.getAlternative_type_id()== S.TIPO_ALTERNATIVA.LIBRE){
                holder.ll_option_input.setVisibility(View.VISIBLE);
                holder.ll_option_image.setVisibility(View.GONE);
                holder.ll_option_select.setVisibility(View.GONE);
                holder.ll_option_range.setVisibility(View.GONE);
                holder.et_data.addTextChangedListener(new TextWatcher(){
                    public void afterTextChanged(Editable s) {
                        String val = holder.et_data.getText().toString().trim();
                        if(val.length()==0)delegate.selectedInput(-1,"");
                        else delegate.selectedInput(position,val);
                    }
                    public void beforeTextChanged(CharSequence s, int start, int count, int after){}
                    public void onTextChanged(CharSequence s, int start, int before, int count){}
                });
                holder.et_data.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if(hasFocus)
                            holder.cv_question.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorPressed));
                    }
                });
            }else if(alt.getImg_url()!=null) {
                holder.ll_option_input.setVisibility(View.GONE);
                holder.ll_option_select.setVisibility(View.GONE);
                holder.ll_option_range.setVisibility(View.GONE);
                holder.tv_name_option.setText(alt.getDescription());
                holder.ll_option_image.setVisibility(View.VISIBLE);
                holder.iv_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                Picasso.with(ctx).load(alt.getImg_url()).placeholder(R.drawable.img_pp).into(holder.iv_image);
            }else if(alt.getMin_value()!=0&&alt.getMax_value()!=0){
                holder.ll_option_input.setVisibility(View.GONE);
                holder.ll_option_select.setVisibility(View.GONE);
                holder.ll_option_image.setVisibility(View.GONE);
                holder.ll_option_range.setVisibility(View.VISIBLE);
                holder.tv_value_range.setText(ctx.getString(R.string.s_seleccione_en_rango)+" "+alt.getMin_value()+" Y "+alt.getMax_value());
                holder.ll_option_range.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.cv_question.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorPressed));
                        SeleccionarRango(holder.tv_value_range,alt.getMin_value(), alt.getMax_value());
                    }
                });
                holder.tv_value_range.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        String val = holder.tv_value_range.getText().toString().trim();
                        if (val.length() == 0) delegate.selectedInput(-1, "");
                        else delegate.selectedInput(position, val);
                    }
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                });
            }else{
                holder.ll_option_input.setVisibility(View.GONE);
                holder.ll_option_image.setVisibility(View.GONE);
                holder.ll_option_range.setVisibility(View.GONE);
                holder.ll_option_select.setVisibility(View.VISIBLE);
                holder.tv_option.setText(alt.getDescription());
            }
            holder.btn_next.setVisibility(View.GONE);
            holder.cv_question.setVisibility(View.VISIBLE);
        }else{
            if(last_question){
                holder.btn_next.setText(ctx.getString(R.string.s_finalizar));
            }
            holder.btn_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delegate.next();
                }
            });
            holder.btn_next.setVisibility(View.VISIBLE);
            holder.cv_question.setVisibility(View.GONE);
        }
    }

    private void SeleccionarRango(final TextView tv,int min,int max) {
        final CharSequence[] items = new CharSequence[max-min+1];
        for (int i=0;i<=max-min;i++)
            items[i] = String.valueOf(i+1);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(ctx.getString(R.string.s_seleccione_rango));
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                tv.setText(items[item]);
                levelDialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.setCancelable(false);
        levelDialog.show();
    }

    @Override
    public int getItemCount() {
        return ds.size()+1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView tv_option,tv_name_option,tv_value_range;
        protected CardView cv_question;
        protected Button btn_next;
        protected EditText et_data;
        protected ImageView iv_image;
        protected LinearLayout ll_question,ll_option_select,ll_option_input,ll_option_image,ll_option_range;
        protected int position;
        protected boolean clickable = false;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_option =  (TextView) itemView.findViewById(R.id.tv_option);
            tv_name_option =  (TextView) itemView.findViewById(R.id.tv_name_option);
            tv_value_range =  (TextView) itemView.findViewById(R.id.tv_value_range);
            cv_question =  (CardView) itemView.findViewById(R.id.cv_question);
            ll_question =  (LinearLayout) itemView.findViewById(R.id.ll_question);
            ll_option_select =  (LinearLayout) itemView.findViewById(R.id.ll_option_select);
            ll_option_input =  (LinearLayout) itemView.findViewById(R.id.ll_option_input);
            ll_option_image =  (LinearLayout) itemView.findViewById(R.id.ll_option_image);
            ll_option_range =  (LinearLayout) itemView.findViewById(R.id.ll_option_range);
            et_data =  (EditText) itemView.findViewById(R.id.et_data);
            btn_next =  (Button) itemView.findViewById(R.id.btn_next);
            iv_image =  (ImageView) itemView.findViewById(R.id.iv_image);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if(clickable)
            delegate.selected(position);
        }
    }
}