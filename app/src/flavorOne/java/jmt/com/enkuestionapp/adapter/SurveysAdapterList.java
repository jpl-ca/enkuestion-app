package jmt.com.enkuestionapp.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.CbOptionSelected;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.SurveyE;

/**
 * Created by JMTech-Android on 16/07/2015.
 *
 * REEMPLAZADO POR SurveyAdapterGrid
 */
public class SurveysAdapterList extends RecyclerView.Adapter<SurveysAdapterList.ViewHolder> {
    private ArrayList<SurveyE> ds;
    private Context ctx;
    public static CbOptionSelected delegate;
    public SurveysAdapterList(Context _ctx, ArrayList<SurveyE> dataArgs, CbOptionSelected callback){
        ds = dataArgs;
        delegate = callback;
        ctx = _ctx;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_survey, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        holder.tv_survey.setText(ds.get(position).getTitle());
        holder.tv_pts.setText(String.valueOf(ds.get(position).getPuntos()));
        int colorBG;
        if(ds.get(position).getSurvey_state_id()==S.SURVEY.ACTIVO)
            colorBG = ctx.getResources().getColor(R.color.colorBl);
        else
            colorBG = ctx.getResources().getColor(R.color.colorGray);
        holder.rl_bg_pts.getBackground().setColorFilter(colorBG, PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public int getItemCount() {
        return ds.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView tv_survey,tv_pts;
        protected RelativeLayout rl_bg_pts;
        protected int position;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_survey =  (TextView) itemView.findViewById(R.id.tv_survey);
            tv_pts =  (TextView) itemView.findViewById(R.id.tv_pts);
            rl_bg_pts =  (RelativeLayout) itemView.findViewById(R.id.rl_bg_pts);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            delegate.selected(position);
        }
    }
}