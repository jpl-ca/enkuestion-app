package jmt.com.enkuestionapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import jmt.com.enkuestionapp.QrCodeGenActivity;
import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.UserE;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class PuntosAdapter extends RecyclerView.Adapter<PuntosAdapter.ViewHolder> {
    private Context ctx;
    private UserE user;
    private String Puntos;
    public PuntosAdapter(Context _ctx, UserE _user){
        ctx = _ctx;
        user = _user;
        Puntos = "0";
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_puntos, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.btn_generar_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = holder.et_data.getText().toString();
                try {
                    int pts = Integer.parseInt(p);
                    if(p.equals("")||pts<1)return;
                    Intent it = new Intent(ctx, QrCodeGenActivity.class);
                    it.putExtra(S.SURVEY.SURVEY_POINTS,String.valueOf(p));
                    it.putExtra(S.USUARIO.DNI,user.getId_doc_number());
                    ctx.startActivity(it);
                }catch (Exception e){}
            }
        });

        holder.et_data.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String p = holder.et_data.getText().toString();
                if (p.length() == 0) {
                    Puntos = "0";
                    return;
                }
                int pts = Integer.parseInt(p);
                if (pts > user.getReward_points()) {
                    if (Puntos.length() <= 0) Puntos = "0";
                    holder.et_data.setText(Puntos);
                    holder.et_data.setSelection(Puntos.length());
                } else Puntos = p;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Puntos = s.toString();
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        holder.tv_puntos.setText(String.valueOf(user.getReward_points()));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        protected Button btn_generar_qr;
        protected TextView tv_puntos;
        protected EditText et_data;
        public ViewHolder(View itemView) {
            super(itemView);
            btn_generar_qr = (Button) itemView.findViewById(R.id.btn_generar_qr);
            tv_puntos = (TextView) itemView.findViewById(R.id.tv_puntos);
            et_data = (EditText) itemView.findViewById(R.id.txt_usar_puntos);
        }
    }
}