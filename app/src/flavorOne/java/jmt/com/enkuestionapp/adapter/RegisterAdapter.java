package jmt.com.enkuestionapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.onRegisterListener;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.util.Util;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class RegisterAdapter extends RecyclerView.Adapter<RegisterAdapter.ViewHolder> {
    public static Activity ctx;
    public static onRegisterListener delegate;
    public RegisterAdapter(Activity _ctx, onRegisterListener callback){
        delegate = callback;
        ctx = _ctx;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_registro, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txt_phone.setText(new Util(ctx).getNumberPhone());
        holder.btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.sp_state.getSelectedItemPosition()==0){
                    delegate.alert(holder.rootLayout,ctx.getString(R.string.s_seleccione_genero));
                    return;
                }
                if(holder.txt_fecha_nacimiento.getText().length()==0){
                    delegate.alert(holder.rootLayout,ctx.getString(R.string.s_seleccione_fecha_nacimiento));
                    return;
                }
                delegate.click(holder.rootLayout, holder.getData());
            }
        });
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements DatePickerDialog.OnDateSetListener {
        protected LinearLayout rootLayout;
        protected Button btn_registrar;
        protected Spinner sp_state;
        protected EditText txt_email,txt_dni,txt_fecha_nacimiento,txt_apellido_usuario,
                txt_nombre_usuario,txt_phone,txt_password,txt_password_connfirm;
        protected int position;
        protected boolean clickable = false;
        String dateToSend = "0000-00-00";
        String dateToShow = "01-01-2000";
        Calendar nowSelected;
        public ViewHolder(View itemView) {
            super(itemView);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayout);
            btn_registrar = (Button) itemView.findViewById(R.id.btn_registrar);
            sp_state = (Spinner) itemView.findViewById(R.id.sp_state);
            txt_email = (EditText) itemView.findViewById(R.id.txt_email);
            txt_phone = (EditText) itemView.findViewById(R.id.txt_phone);
            txt_dni = (EditText) itemView.findViewById(R.id.txt_dni);
            txt_fecha_nacimiento = (EditText) itemView.findViewById(R.id.txt_fecha_nacimiento);
            txt_apellido_usuario = (EditText) itemView.findViewById(R.id.txt_apellido_usuario);
            txt_nombre_usuario = (EditText) itemView.findViewById(R.id.txt_nombre_usuario);
            txt_password = (EditText) itemView.findViewById(R.id.txt_pass);
            txt_password_connfirm = (EditText) itemView.findViewById(R.id.txt_pass_confirm);
            nowSelected = Calendar.getInstance();
            txt_fecha_nacimiento.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            ViewHolder.this,
                            nowSelected.get(Calendar.YEAR),
                            nowSelected.get(Calendar.MONTH),
                            nowSelected.get(Calendar.DAY_OF_MONTH)
                    );
                    dpd.show(ctx.getFragmentManager(), "Datepickerdialog");
                }
            });
        }
        public UserE getData(){
            UserE user = new UserE();
            user.setFirst_name(txt_nombre_usuario.getText().toString());
            user.setLast_name(txt_apellido_usuario.getText().toString());
            user.setBirth_date(dateToSend);
            user.setId_doc_number(txt_dni.getText().toString());
            user.setEmail(txt_email.getText().toString());
            user.setPhone(txt_phone.getText().toString());
            int posGender = sp_state.getSelectedItemPosition()==0?0:sp_state.getSelectedItemPosition()-1;
            user.setGender(posGender);
            user.setPassword(txt_password.getText().toString());
            user.setPassword_confirmation(txt_password_connfirm.getText().toString());
            return user;
        }
        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
            dateToShow = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
            dateToSend = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
            txt_fecha_nacimiento.setText(dateToShow);
        }
    }
}