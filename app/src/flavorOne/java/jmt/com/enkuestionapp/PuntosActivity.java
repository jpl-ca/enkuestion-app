package jmt.com.enkuestionapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.google.gson.GsonBuilder;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.service.LogOutTask;
import jmt.com.enkuestionapp.util.Util;

public class PuntosActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private AppBarLayout appbar;
    private Util util;
    private UserE user;
    CollapsingToolbarLayout collapsingToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puntos);
        setupComponents();
        adapter = new PuntosAdapter(this,user);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setupComponents(){
        user = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(new JmStore(this).get(S.USUARIO.USUARIO),UserE.class);
        util = new Util(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appbar = (AppBarLayout) findViewById(R.id.app_bar);
        util.resizeAppBar(appbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getString(R.string.s_puntos));
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBarLarge);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1Large);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_toolbar_bg);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.rv_usuario);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pts, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent it = new Intent(this,InicioActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
            finish();
        }
        if (id == R.id.action_profile) {
            startActivity(new Intent(this,DatosUsuarioActivity.class));
        }
        if (id == R.id.action_close) {
            new LogOutTask(this, new CallbackRequest() {
                @Override
                public void processFinish(String msj) {
                }
                @Override
                public void processFinish(ResponseE response) {
                    Intent it = new Intent(PuntosActivity.this,AccesoActivity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(it);
                    finish();
                    new JmStore(PuntosActivity.this).save(S.USUARIO.USUARIO,null);
                }
            }).execute();
        }
        return super.onOptionsItemSelected(item);
    }
}