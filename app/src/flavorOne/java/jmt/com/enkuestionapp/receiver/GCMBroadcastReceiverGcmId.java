package jmt.com.enkuestionapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.service.GcmIdTask;

public class GCMBroadcastReceiverGcmId extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        String regId = intent.getExtras().getString("registration_id");
        if(regId==null)return;
        new JmStore(context).save(S.GCMID, regId);
        new GcmIdTask(context).execute(regId);
    }
}