package jmt.com.enkuestionapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import jmt.com.enkuestionapp.adapter.RegisterAdapter;
import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.interfaz.onRegisterListener;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.service.RegistroCuentaTask;
import jmt.com.enkuestionapp.util.Util;

public class RegistroCuentaActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private AppBarLayout appbar;
    private Util util;
    CollapsingToolbarLayout collapsingToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_cuenta);
        setupComponents();
        adapter = new RegisterAdapter(this,new onRegisterListener(){
            @Override
            public void click(final View rootLayout,UserE user){
                String usr = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user);
                try {
                    new RegistroCuentaTask(RegistroCuentaActivity.this, new CallbackRequest() {
                        @Override
                        public void processFinish(String msj) {
                            util.showToast(rootLayout,msj);
                        }
                        @Override
                        public void processFinish(ResponseE response) {
                            util.showToast(rootLayout,"Sus datos se registraron con exito!");
                            siguiente();
                        }
                    }).execute(new JSONObject(usr));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void alert(final View rootLayout, String msj){
                util.showToast(rootLayout,msj);
            }
        });
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void siguiente(){
        Intent it = new Intent(RegistroCuentaActivity.this,InicioActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);
        finish();
    }

    private void setupComponents(){
        util = new Util(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_logo);
//        toolbar.setNavigationIcon(R.mipmap.ic_logo_app_bar);
        setSupportActionBar(toolbar);
        appbar = (AppBarLayout) findViewById(R.id.app_bar);
        util.resizeAppBar(appbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getString(R.string.s_crear_cuenta));
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBarDark);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1Dark);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_toolbar_bg);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.rv_usuario);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }
}