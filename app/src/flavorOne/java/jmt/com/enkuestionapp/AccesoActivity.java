package jmt.com.enkuestionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
public class AccesoActivity extends AppCompatActivity{
    private Button btn_iniciar_Sesion,btn_registro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceso);
        setupComponents();
    }
    private void setupComponents(){
        btn_iniciar_Sesion = (Button)findViewById(R.id.btn_iniciar_sesion);
        btn_registro = (Button)findViewById(R.id.btn_registro);
        btn_iniciar_Sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccesoActivity.this,LoginActivity.class));
            }
        });
        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccesoActivity.this,RegistroCuentaActivity.class));
            }
        });
    }
}