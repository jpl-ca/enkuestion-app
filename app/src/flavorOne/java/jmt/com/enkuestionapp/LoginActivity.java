package jmt.com.enkuestionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.google.gson.GsonBuilder;
import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.service.LoginTask;
import jmt.com.enkuestionapp.util.HttpApi;
import jmt.com.enkuestionapp.util.Util;

public class LoginActivity extends AppCompatActivity{
    private EditText txt_usuario,txt_pass;
    private RelativeLayout root_layout;
    private FloatingActionButton btnSend;
    HttpApi http;
    UserE user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupComponents();
    }

    private void setupComponents(){
        user = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(new JmStore(this).get(S.USUARIO.USUARIO),UserE.class);
//        String user_doc = user!=null?user.getId_doc_number():"61904644";
        String user_doc = "";
        http = new HttpApi();
        root_layout = (RelativeLayout) findViewById(R.id.rootLayout);
        txt_usuario = (EditText) findViewById(R.id.txt_user);
        txt_usuario.setText(user_doc);
        txt_pass = (EditText) findViewById(R.id.txt_pass);
        txt_pass.setText("");
        btnSend = (FloatingActionButton) findViewById(R.id.fab);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciarSesion();
            }
        });
    }

    private void iniciarSesion(){
        enableControl(false);
        new LoginTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {
                new Util(LoginActivity.this).showToast(root_layout,msj);
                enableControl(true);
            }
            @Override
            public void processFinish(ResponseE response) {
                ingresar();
            }
        }).execute(txt_usuario.getText().toString(),txt_pass.getText().toString());
    }

    private void enableControl(boolean b){
        txt_usuario.setEnabled(b);
        txt_pass.setEnabled(b);
        btnSend.setEnabled(b);
    }
    private void ingresar(){
        Intent it = new Intent(LoginActivity.this,InicioActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);
        finish();
    }
}