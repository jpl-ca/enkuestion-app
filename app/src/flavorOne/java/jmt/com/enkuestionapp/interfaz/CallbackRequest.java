package jmt.com.enkuestionapp.interfaz;

import jmt.com.enkuestionapp.model.entity.ResponseE;

/**
 * Created by JMTech-Android on 23/07/2015.
 */
public interface CallbackRequest {
    void processFinish(String msj);
    void processFinish(ResponseE response);
}