package jmt.com.enkuestionapp.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.SensorManager;
import android.media.Image;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.onLoadImageListener;
import jmt.com.enkuestionapp.model.JmStore;

/**
 * Created by JMTech-Android on 22/07/2015.
 */
public class Util {
    Activity activity;
    JmStore jmstore;
//    String pth_file;
//    onLoadImageListener cb_load_image;
    public Util(){}
    public Util(Activity activity){
        this.activity = activity;
        jmstore = new JmStore(activity);
    }

    public DisplayMetrics metricsDevice(){
        return activity.getResources().getDisplayMetrics();
    }
    public int GCD(int a, int b) { return b==0 ? a : GCD(b, a%b); }
    public void resizeAppBar(AppBarLayout appbar){
        DisplayMetrics dm = metricsDevice();
        float heightD = dm.heightPixels;
        float widthD = dm.widthPixels;
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams)appbar.getLayoutParams();
        lp.height = (((int)widthD)/16)*9;
    }
    public int heightFromScreen(int ratioWidth,int ratioHeight){
        DisplayMetrics dm = metricsDevice();
        float widthD = dm.widthPixels;
        return  (((int)widthD)/ratioWidth)*ratioHeight;
    }

    public void showToast(View parentLayout,String txt){
        Snackbar
                .make(parentLayout, txt, Snackbar.LENGTH_LONG)
                .show();
    }
    public String getNumberPhone(){
        TelephonyManager telemamanger = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        String getSimSerialNumber = telemamanger.getLine1Number();
        return getSimSerialNumber;
    }

    public static String format_date_time(){
        return "yyyy-MM-dd kk:mm:ss";
    }
    public static String format_date(){
        return "dd-MM-yyyy";
    }
    public static String nowDateTime(){
        return (String) DateFormat.format(format_date_time(), new Date());
    }
    public static String nowDateTimeISO8601(){
        return (String) DateFormat.format("yyyy-MM-dd'T'kk:mm:ss'Z'", new Date());
    }
    public static String nowDate(){
        return (String) DateFormat.format(format_date(), new Date());
    }

    public boolean isPhone(){
        TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        SensorManager manager = (SensorManager) activity.getSystemService(activity.SENSOR_SERVICE);
        WifiManager wifiManager = (WifiManager) activity.getSystemService(activity.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String carrierName = telephonyManager.getNetworkOperatorName();

        System.out.println("-------------------");
        System.out.println(carrierName);
        if(carrierName.toLowerCase().equals("")||carrierName.toLowerCase().equals("android"))return false;//GenyMotion
        System.out.println("-------------------");
        System.out.println(telephonyManager.getDeviceId());
        if(telephonyManager.getDeviceId().contains("000000000000000"))return false;//GenyMotion
        System.out.println("-------------------");
        System.out.println(Build.FINGERPRINT);
        if(Build.FINGERPRINT.toLowerCase().contains("generic"))return false;//GenyMotion
        System.out.println("-------------------");
        System.out.println(Build.BRAND);
        if(Build.BRAND.toLowerCase().equals("generic"))return false;//GenyMotion
        System.out.println("-------------------");
        System.out.println(Build.HARDWARE);
        if(Build.HARDWARE.toLowerCase().equals("unknown"))return false;//BlueStack

        String hw = "";
        hw += "A:"+wifiInfo.toString()+"\n";
        hw += "B:"+wifiInfo.getSSID()+"\n";
        hw +=
                telephonyManager.getDeviceId()+"\n"+
                        "'O:"+carrierName+"'\n\n"+
                        Build.FINGERPRINT+"\n\n"+
                        Build.BRAND+"\n\n"+
                        Build.HARDWARE+"\n"+
                        Build.PRODUCT+"\n"+
                        Build.MODEL+"\n";
        return true;
    }

//    public void setImage(onLoadImageListener delegate, int id,String url){
//        pth_file = "JM/encuesta000"+id+"."+(url.endsWith("png")?"png":"jpg");
//        cb_load_image = delegate;
//        File file = new File(Environment.getExternalStorageDirectory().getPath() + pth_file);
//        if(file.exists()){
//            Bitmap bmp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + pth_file);
//            System.out.println("Archivo ya existe!!!");
//            System.out.println("Archivo ya existe!!!");
//            System.out.println("Archivo ya existe!!!");
//            delegate.load(bmp);
//            return;
//        }
//        Picasso.with(activity).load(url).into(target);
//    }
//    Target target = new Target() {
//        @Override
//        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//            cb_load_image.load(bitmap);
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    File file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + pth_file);
//                    File parent = file.getParentFile();
//                    if (parent != null) parent.mkdirs();
//                    try {
//                        file.createNewFile();
//                        FileOutputStream ostream = new FileOutputStream(file);
//                        bitmap.compress(Bitmap.CompressFormat.JPEG,100,ostream);
//                        ostream.close();
//                    }
//                    catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }).start();
//        }
//        @Override
//        public void onBitmapFailed(Drawable errorDrawable) {}
//        @Override
//        public void onPrepareLoad(Drawable placeHolderDrawable) {}
//    };
}