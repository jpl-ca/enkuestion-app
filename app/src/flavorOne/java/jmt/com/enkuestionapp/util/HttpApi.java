package jmt.com.enkuestionapp.util;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jmt.com.enkuestionapp.EncuestaApplication;
/**
 * Created by JMTech-Android on 23/07/2015.
 *  SIN USO
 *  SIN USO
 *  SIN USO
 *  SIN USO
 *  SIN USO
 */
public class HttpApi {
    String url_service;
    public HttpApi(){
        url_service = "http://jmwebserver.ddns.net";
    }
    public void GET(String url,Response.Listener<String> listener,Response.ErrorListener errorListener){
        url = url_service + url;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,listener,errorListener){
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                EncuestaApplication.getInstance().checkSessionCookie(response.headers);
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();
                if (headers == null || headers.equals(Collections.emptyMap()))
                    headers = new HashMap<String, String>();
                EncuestaApplication.getInstance().addSessionCookie(headers);
                return headers;
            }
        };
        EncuestaApplication.getInstance().getRequestQueue().add(stringRequest);
    }
    public void POST(String url,final Map<String, String>  params, Response.Listener<String> listener,Response.ErrorListener errorListener){
        url = url_service + url;
        System.out.println(url);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,listener,errorListener){
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                EncuestaApplication.getInstance().checkSessionCookie(response.headers);
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();
                if (headers == null || headers.equals(Collections.emptyMap()))
                    headers = new HashMap<String, String>();
                EncuestaApplication.getInstance().addSessionCookie(headers);
                return headers;
            }
        };
        EncuestaApplication.getInstance().getRequestQueue().add(postRequest);
    }
}