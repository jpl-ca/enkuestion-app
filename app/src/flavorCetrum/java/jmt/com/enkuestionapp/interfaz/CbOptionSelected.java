package jmt.com.enkuestionapp.interfaz;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public interface CbOptionSelected {
    void selected(int position);
    void selectedInput(int position, String data);
    void next();
}