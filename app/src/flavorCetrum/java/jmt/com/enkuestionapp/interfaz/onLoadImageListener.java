package jmt.com.enkuestionapp.interfaz;

import android.graphics.Bitmap;
import android.view.View;

import jmt.com.enkuestionapp.model.entity.UserE;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public interface onLoadImageListener {
    void load(Bitmap bm);
}
