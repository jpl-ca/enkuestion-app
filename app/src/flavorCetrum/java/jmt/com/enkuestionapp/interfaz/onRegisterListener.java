package jmt.com.enkuestionapp.interfaz;

import android.view.View;

import jmt.com.enkuestionapp.model.entity.UserE;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public interface onRegisterListener {
    void click(View v, UserE data);
    void alert(View v, String msj);
}
