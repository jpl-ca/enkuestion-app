package jmt.com.enkuestionapp;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.service.CheckTask;
import jmt.com.enkuestionapp.service.GCMRegister;
import jmt.com.enkuestionapp.util.Util;

public class SplashActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*
//            TODO comentado para probar el app en emuladores
         */
        continuar();
//        if(new Util(this).isPhone())continuar();
//        else ((TextView)findViewById(R.id.tv)).setVisibility(View.VISIBLE);
    }

    private void continuar(){
        new CheckTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, AccesoActivity.class));
                        finish();
                    }
                }, 1000);
            }
            @Override
            public void processFinish(ResponseE response) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, InicioActivity.class));
                        finish();
                    }
                }, 1000);
            }
        }).execute();
    }
}