package jmt.com.enkuestionapp.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 31/07/2015.
 */
public class AnswerE implements Serializable {
    @Expose
    int answer_alternative_id;
    @Expose
    String raw;

    @Expose
    String comment;

    public AnswerE(int answer_alternative_id,String raw){
        this.answer_alternative_id = answer_alternative_id;
        this.raw = raw;
    }
    public AnswerE(int answer_alternative_id){
        this.answer_alternative_id = answer_alternative_id;
    }
    public void setComment(String _comment){
        comment = _comment;
    }
    @Expose
    String pregunta;
    public AnswerE(int answer_alternative_id,String raw,String pregunta){
        this.answer_alternative_id = answer_alternative_id;
        this.raw = raw;
        this.pregunta = pregunta;
    }
}