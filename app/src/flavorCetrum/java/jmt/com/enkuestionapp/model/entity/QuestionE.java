package jmt.com.enkuestionapp.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class QuestionE  implements Serializable {
    @SerializedName("id")
    @Expose
    int question_id;
    @Expose
    int survey_id;
    @Expose
    String title;
    @Expose
    String help_text;
    @SerializedName("question_type_id")
    @Expose
    int question_type;
    @Expose
    int mandatory;
    @SerializedName("answer_alternatives")
    @Expose
    ArrayList<AlternativeE> alternative;
    @Expose
    @SerializedName("section")
    SectionE section;
    boolean state;

    public QuestionE(int question_id,int survey_id,String title,String help_text,int question_type,int mandatory,boolean state){
        this.question_id = question_id;
        this.survey_id = survey_id;
        this.title = title;
        this.help_text = help_text;
        this.question_type = question_type;
        this.mandatory = mandatory;
        this.state = state;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(int survey_id) {
        this.survey_id = survey_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHelp_text() {
        return help_text;
    }

    public void setHelp_text(String help_text) {
        this.help_text = help_text;
    }

    public int getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(int question_type) {
        this.question_type = question_type;
    }

    public int getMandatory() {
        return mandatory;
    }

    public void setMandatory(int mandatory) {
        this.mandatory = mandatory;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public ArrayList<AlternativeE> getAlternative() {
        return alternative;
    }

    public void setAlternative(ArrayList<AlternativeE> alternative) {
        this.alternative = alternative;
    }

    public boolean isState() {
        return state;
    }

    public SectionE getSection() {
        return section;
    }

    public String getSectionName() {
        return section.getName();
    }

    public void setSection(SectionE section) {
        this.section = section;
    }

    class SectionE implements Serializable{
        @Expose int id;
        @Expose String name;
        @Expose String description;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}