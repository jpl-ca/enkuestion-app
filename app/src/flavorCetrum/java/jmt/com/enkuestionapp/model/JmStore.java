package jmt.com.enkuestionapp.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import jmt.com.enkuestionapp.model.entity.AlternativeE;
import jmt.com.enkuestionapp.model.entity.QuestionE;
import jmt.com.enkuestionapp.model.entity.SurveyE;

/**
 * Created by JMTech-Android on 16/07/2015.
 */
public class JmStore {

    SharedPreferences DB;
    public JmStore(Context ctx){
        DB= ctx.getSharedPreferences("sp_encuesta_jm",Context.MODE_PRIVATE);
    }
    public void save(String key,String value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public String get(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }
}