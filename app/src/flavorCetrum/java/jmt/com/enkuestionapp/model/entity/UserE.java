package jmt.com.enkuestionapp.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public class UserE implements Serializable {
    @SerializedName("id")
    @Expose
    String user_id;
    @Expose
    String first_name;
    @Expose
    String last_name;
    @Expose
    String id_doc_number;
    @Expose
    String phone;
    @Expose
    int gender;
    @Expose
    String birth_date;
    @Expose
    String email;
    @Expose
    String last_position;
    @Expose
    int reward_points;
    @Expose
    int has_trial_period;
    @Expose
    String password;
    @Expose
    String password_confirmation;

    public String getUser_id() {
        return user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getFirst_name() {
        return first_name;
    }
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
    public String getLast_name() {
        return last_name;
    }
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public int getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }
    public String getBirth_date() {
        return birth_date;
    }
    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getLast_position() {
        return last_position;
    }
    public void setLast_position(String last_position) {
        this.last_position = last_position;
    }
    public int getReward_points() {
        return reward_points;
    }
    public void setReward_points(int reward_points) {
        this.reward_points = reward_points;
    }
    public int getHas_trial_period() {
        return has_trial_period;
    }
    public void setHas_trial_period(int has_trial_period) {this.has_trial_period = has_trial_period;}
    public String getId_doc_number() {
        return id_doc_number;
    }
    public void setId_doc_number(String id_doc_number) {
        this.id_doc_number = id_doc_number;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }
}