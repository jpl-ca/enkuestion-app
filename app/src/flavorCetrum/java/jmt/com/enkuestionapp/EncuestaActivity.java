package jmt.com.enkuestionapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import jmt.com.enkuestionapp.adapter.OptionsAdapter;
import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.interfaz.CbOptionSelected;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.AlternativeE;
import jmt.com.enkuestionapp.model.entity.AnswerE;
import jmt.com.enkuestionapp.model.entity.QuestionE;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.service.SendAnswerSurveyTask;
import jmt.com.enkuestionapp.util.Util;

public class EncuestaActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Button btn_listo;
    private LinearLayout ll_question,ll_finish;
    private TextView tv_question,tv_help,tv_title_b,tv_title_a,tv_num_question;
    private CardView cv_title;
    private JmStore jmstore;
    private int question_number;
    private ArrayList<QuestionE> question_list;
    private ArrayList<AnswerE> answer_list;
    private QuestionE current_question;
    private int QUESTION_SIZE=0,POINTS = 0;
    private String nombre_encuesta="";
    private long survery_id=0;
    boolean is_last_question;
    int TIME_TRANSITION = 0;
    public static int HALF_POSITION = 3;
    Util util;
    Gson gson;
    HashSet<Integer> SelectedItem;
    AnswerE AnswerTMP;
    MenuItem item_refresh;
    AlertDialog.Builder builder;
    Intent returnIntent;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);
        jmstore = new JmStore(this);
        AnswerTMP = null;
        builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        Intent intent = getIntent();
        POINTS = intent.getIntExtra(S.SURVEY.SURVEY_POINTS,0);
        nombre_encuesta = intent.getStringExtra(S.SURVEY.SURVEY_NAME);
        survery_id = intent.getIntExtra(S.SURVEY.SURVEY_ID,0);
        question_list = (ArrayList<QuestionE>) intent.getSerializableExtra(S.SURVEY.QUESTION_LIST);
        QUESTION_SIZE = question_list.size();
        question_number = 0;
        TIME_TRANSITION = getResources().getInteger(R.integer.swing_anim_time);
        setupComponents();
        loadQuestion();
    }

    private void loadQuestion(){
        final Animation ur = AnimationUtils.loadAnimation(this, R.anim.swing_in_right);
        current_question = question_list.get((question_number++) % QUESTION_SIZE);
        tv_num_question.setText(String.valueOf(question_number));
        tv_question.setText(current_question.getTitle());
        tv_help.setText(current_question.getHelp_text());
        String section_name = current_question.getSectionName();
        if(section_name==null||section_name.length()==0)
            section_name = nombre_encuesta;
        toolbar.setTitle(section_name);
        is_last_question=false;
        if(question_number==QUESTION_SIZE){
            is_last_question=true;
        }
        if(question_number>1){
            ll_question.setVisibility(View.VISIBLE);
            ll_question.startAnimation(ur);
        }
        final ArrayList<AlternativeE> alternativeList = current_question.getAlternative();

        adapter = new OptionsAdapter(this,is_last_question,alternativeList, new CbOptionSelected(){
            @Override
            public void selected(int position) {
                if(SelectedItem.size()==1&&SelectedItem.contains(position))
                    // Es la unica opcion seleccionada
                    return;
                if(current_question.getQuestion_type()== S.TIPO_PREGUNTA.SEL_UNICA){
                    // Desseleccionar las opciones y eliminar de la lista
                    for (int s : SelectedItem)
                        alternativeList.get(s).setSelected(false);
                    SelectedItem.clear();
                }
                alternativeList.get(position).setSelected(!alternativeList.get(position).isSelected());
                if(SelectedItem.contains(position))SelectedItem.remove(position);
                else SelectedItem.add(position);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void selectedInput(int position,String data){
                if(position==-1){
                    AnswerTMP = null;
                    SelectedItem.remove(position);
                }else{
                    AnswerTMP = new AnswerE(alternativeList.get(position).getAlternative_id(),data);
                    SelectedItem.add(position);
                }
            }
            @Override
            public void next(){
                if(SelectedItem.size()==0){
                    util.showToast((RelativeLayout)findViewById(R.id.rootLayout),getString(R.string.s_seleccione_una_opcion));
                    return;
                }
                if(SelectedItem.size()==1){
                    int pos = 0;
                    for (int position : SelectedItem)
                        pos = position;
                    if(pos<HALF_POSITION&&OptionsAdapter.Justificacion.length()<3){
                        util.showToast((RelativeLayout) findViewById(R.id.rootLayout), getString(R.string.s_obligatorio_justificar_su_calificacion));
                        return;
                    }
                }
                InputMethodManager imm =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
                for (int position : SelectedItem){
                    String justificacion = OptionsAdapter.Justificacion;
                    if(alternativeList.get(position).getAlternative_type_id()==S.TIPO_ALTERNATIVA.LIBRE&&AnswerTMP!=null){
                        if(justificacion.length()>0)
                            AnswerTMP.setComment(justificacion);
                        answer_list.add(AnswerTMP);
                    }
                    else{
                        AnswerTMP = new AnswerE(alternativeList.get(position).getAlternative_id());
                        if(justificacion.length()>0)
                            AnswerTMP.setComment(justificacion);
                        answer_list.add(AnswerTMP);
                    }
                }
                SelectedItem.clear();
                AnswerTMP = null;
                OptionsAdapter.Justificacion = "";
                nextQuestion();
            }
        });
        recyclerView.setAdapter(adapter);
        btn_listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void prepareToSend(){
        item_refresh.setActionView(new ProgressBar(EncuestaActivity.this));
        tv_title_a.setText(getString(R.string.s_enviando));
        new SendAnswerSurveyTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {}
            @Override
            public void processFinish(ResponseE response) {
                item_refresh.setActionView(null);
                try {
                    JSONObject job = new JSONObject(response.getData());
                    returnIntent.putExtra(getString(R.string.s_mensaje),job.getString("message"));
                    if(job.has("success") && job.getBoolean("success"))
                        setResult(RESULT_OK,returnIntent);
                    else
                        setResult(RESULT_CANCELED,returnIntent);
                } catch (JSONException e) {e.printStackTrace();}
                finish();
            }
        },survery_id).execute(answer_list);
    }

    public void finished(){
        final Animation ur = AnimationUtils.loadAnimation(this, R.anim.swing_in_right);
        ll_finish.setVisibility(View.VISIBLE);
        ll_finish.startAnimation(ur);
        prepareToSend();

    }
    public void nextQuestion(){
        final Animation ul = AnimationUtils.loadAnimation(this, R.anim.swing_out_left);
        ll_question.startAnimation(ul);
        ll_question.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(is_last_question)
                    finished();
                else
                    loadQuestion();
            }
        }, TIME_TRANSITION);
    }
    private void setupComponents(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(nombre_encuesta);
        setSupportActionBar(toolbar);
        util = new Util();
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        SelectedItem = new HashSet<>();
        answer_list = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.rv_options);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        ll_question = (LinearLayout) findViewById(R.id.ll_question);
        ll_finish = (LinearLayout) findViewById(R.id.ll_finish);
        cv_title= (CardView) findViewById(R.id.cv_title);
        tv_num_question = (TextView) findViewById(R.id.tv_num_question);
        tv_help= (TextView) findViewById(R.id.tv_help);
        tv_title_a= (TextView) findViewById(R.id.tv_title_a);
        tv_title_b= (TextView) findViewById(R.id.tv_title_b);
        tv_question= (TextView) findViewById(R.id.tv_question);
        btn_listo= (Button) findViewById(R.id.btn_listo);
        btn_listo.setVisibility(View.GONE);
        returnIntent = new Intent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_survey, menu);
        item_refresh = menu.findItem(R.id.action_survey);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_survey){
            setResult(RESULT_CANCELED,returnIntent);
                areyousure();
        }
        return super.onOptionsItemSelected(item);
    }
    public void areyousure() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        builder.setMessage(getString(R.string.s_cancelar_encuesta)).setPositiveButton("SI", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    @Override
    public void onBackPressed() {
        areyousure();
    }
}