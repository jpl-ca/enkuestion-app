package jmt.com.enkuestionapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;

import jmt.com.enkuestionapp.adapter.SurveysAdapterGrid;
import jmt.com.enkuestionapp.adapter.SurveysAdapterList;
import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.interfaz.CbOptionSelected;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.SurveyE;
import jmt.com.enkuestionapp.service.EncuestaTask;
import jmt.com.enkuestionapp.service.GCMRegister;
import jmt.com.enkuestionapp.service.LocationFusedService;
import jmt.com.enkuestionapp.service.LogOutTask;

public class InicioActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private JmStore jmstore;
    private RelativeLayout root_layout,rv_bg_list;
    private TextView tv_sin_encuesta;
    private int question_number;
    private ArrayList<SurveyE> survey_list;
    AlertDialog.Builder builder;
    Gson gson;
    MyReceiver mReceiver;
    private SwipeRefreshLayout mSwipeRefreshLV = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        setupComponents();
        jmstore = new JmStore(this);
        question_number = 0;
        startService(new Intent(this, GCMRegister.class));
        //TODO comentado para centrum
//        startService(new Intent(this, LocationFusedService.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mReceiver = new MyReceiver();
        IntentFilter filter = new IntentFilter(S.BROADCAST.NEW_SURVERY);
        registerReceiver(mReceiver, filter);
        refreshSurveys();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReceiver);
    }

    private void showListaEncuesta(){
        adapter = new SurveysAdapterGrid(this,survey_list, new CbOptionSelected(){
            @Override
            public void selected(int position) {
                startSurvey(position);
            }
            @Override
            public void selectedInput(int position,String data){}
            @Override
            public void next(){}
        });
        recyclerView.setAdapter(adapter);
    }
    private void startSurvey(int position){
        SurveyE survey = survey_list.get(position);
        Intent it = new Intent(InicioActivity.this,EncuestaActivity.class);
        it.putExtra(S.SURVEY.SURVEY_ID,survey.getSurvey_id());
        it.putExtra(S.SURVEY.SURVEY_NAME,survey.getTitle());
        it.putExtra(S.SURVEY.SURVEY_POINTS,survey.getPuntos());
        it.putExtra(S.SURVEY.QUESTION_LIST,survey.getQuestions());
        startActivityForResult(it, S.SURVEY.CODE_RESULT);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == S.SURVEY.CODE_RESULT) {
            if(data==null||data.getExtras()==null)return;
            String result=data.getStringExtra(getString(R.string.s_mensaje));
            if(resultCode == RESULT_OK){
                builder.setTitle(getString(R.string.s_felicitaciones));
            }
            if (resultCode == RESULT_CANCELED) {
                builder.setTitle(getString(R.string.s_sin_exito));
            }
            builder.setMessage(result).setPositiveButton("OK", dialogClickListener).show();
        }
    }
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {}
    };
    private void setupComponents(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        toolbar.setTitle(getString(R.string.s_encuestas));
        setSupportActionBar(toolbar);
        builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        tv_sin_encuesta = (TextView) findViewById(R.id.tv_sin_encuesta);
        root_layout = (RelativeLayout) findViewById(R.id.rootLayout);
        rv_bg_list = (RelativeLayout) findViewById(R.id.rv_bg_list);
        recyclerView = (RecyclerView) findViewById(R.id.rv_surveys);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
        GridLayoutManager gLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gLayoutManager);
        mSwipeRefreshLV= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLV);
        mSwipeRefreshLV.setColorSchemeResources(android.R.color.holo_orange_dark, android.R.color.holo_red_dark, android.R.color.holo_blue_dark, android.R.color.holo_green_dark);
        mSwipeRefreshLV.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshSurveys();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void refreshSurveys() {
        new EncuestaTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {
                if (mSwipeRefreshLV.isRefreshing()) {
                    mSwipeRefreshLV.setRefreshing(false);
                }
            }
            @Override
            public void processFinish(ResponseE response) {
                survey_list = gson.fromJson(response.getData(), new TypeToken<ArrayList<SurveyE>>(){}.getType());
                System.out.println("RRR--->"+response.getData());
                if(survey_list.size()==0){
                    rv_bg_list.setVisibility(View.VISIBLE);
                    tv_sin_encuesta.setText(getString(R.string.s_pronto_nuevas_encuestas));
                    recyclerView.setVisibility(View.GONE);
                }else{
                    rv_bg_list.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    showListaEncuesta();
                }
                if (mSwipeRefreshLV.isRefreshing()) {
                    mSwipeRefreshLV.setRefreshing(false);
                }
            }
        }).execute();
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_profile) {
            startActivity(new Intent(this,DatosUsuarioActivity.class));
        }
        if (id == R.id.action_close) {
            new LogOutTask(this, new CallbackRequest() {
                @Override
                public void processFinish(String msj) {
                }
                @Override
                public void processFinish(ResponseE response) {
                    Intent it = new Intent(InicioActivity.this,AccesoActivity.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(it);
                    finish();
                    jmstore.save(S.USUARIO.USUARIO,null);
                }
            }).execute();
        }
        return super.onOptionsItemSelected(item);
    }
    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshSurveys();
        }
    }
}