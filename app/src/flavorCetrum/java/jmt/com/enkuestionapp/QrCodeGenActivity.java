package jmt.com.enkuestionapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.util.Util;

public class QrCodeGenActivity extends AppCompatActivity{
    private TextView tv_num_pts;
    private ImageView iv_qr_code;
    private Button btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_qr_code);
        setupComponents();
    }
    private void setupComponents(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_logo_app_bar);
        setSupportActionBar(toolbar);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        tv_num_pts = (TextView)findViewById(R.id.tv_num_pts);
        iv_qr_code = (ImageView)findViewById(R.id.iv_qr_code);
        btn_ok = (Button)findViewById(R.id.btn_ok);
        Intent intent = getIntent();
        String Puntos = intent.getStringExtra(S.SURVEY.SURVEY_POINTS);
        String dni = intent.getStringExtra(S.USUARIO.DNI);
        tv_num_pts.setText(Puntos+" "+getString(R.string.s_puntos));
        String CodeGen = dni+";"+Puntos+";"+ Util.nowDateTimeISO8601()+";";
        Bitmap bm = null;
        try {
            bm = encodeAsBitmap(CodeGen, BarcodeFormat.QR_CODE, width, width);
            iv_qr_code.setImageBitmap(bm);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = EncodeHintType.MARGIN.name();
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.MARGIN, 0);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height,Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
}