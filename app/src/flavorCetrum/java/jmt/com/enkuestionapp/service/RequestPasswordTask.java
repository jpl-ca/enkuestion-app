package jmt.com.enkuestionapp.service;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONObject;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class RequestPasswordTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    public RequestPasswordTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            jo.put("email", params[0]);
            Z = new OkHttp(ctx).makePostRequest("/api/auth/request-password",jo);
            System.out.println("-----|>");
            System.out.println(Z.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            delegate.processFinish(response);
        }
        else {
            delegate.processFinish(response.getMassege());
        }
    }
}