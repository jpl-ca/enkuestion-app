package jmt.com.enkuestionapp.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import jmt.com.enkuestionapp.InicioActivity;
import jmt.com.enkuestionapp.LoginActivity;
import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.receiver.GCMBroadcastReceiver;

/**
 * Created by JMTech-Android on 04/08/2015.
 */
public class GcmIntentService extends IntentService{
    public int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    Context ctx;
    public GcmIntentService() {
        super("GcmIntentService");
        ctx = this;
    }

    public static final String TAG = "GCMNotificationIntentService";
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ctx);
        String messageType = gcm.getMessageType(intent);
        if (extras != null) {
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR .equals(messageType)) {
                    System.out.println("Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED .equals(messageType)) {
                    System.out.println("Deleted messages on server: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    System.out.println("***********W************");
                    System.out.println(extras.toString());
                    System.out.println("***********W************");
                    sendNotification(extras.getString("message"));
                    Intent inboxIntent = new Intent(S.BROADCAST.NEW_SURVERY);
                    sendBroadcast(inboxIntent);
                }
            }
        }
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void sendNotification(String msj) {
        mNotificationManager = (NotificationManager) ctx.getSystemService(NOTIFICATION_SERVICE);
        String us = new JmStore(ctx).get(S.USUARIO.USUARIO);
        Intent intent = new Intent(ctx, InicioActivity.class);
        if(us==null||us.length()==0)
            intent = new Intent(ctx, LoginActivity.class);
        NotificationCompat.Builder notifB =
                new NotificationCompat.Builder(ctx)
                        .setContentTitle(getString(R.string.s_encuestas))
                        .setContentText(msj)
                        .setSmallIcon(R.mipmap.ic_logo);
        PendingIntent pIntent = PendingIntent.getActivity(ctx,NOTIFICATION_ID, intent, 0);
        Notification notification = notifB.setContentIntent(pIntent).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }
}