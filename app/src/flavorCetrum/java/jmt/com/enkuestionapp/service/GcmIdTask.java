package jmt.com.enkuestionapp.service;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.util.OkHttp;

public class GcmIdTask extends AsyncTask<String, String, ResponseE> {
    Context ctx;
    public GcmIdTask(Context ctx){
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            jo.put("gcm_id",params[0]);
            System.out.println("Enviando:"+jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/api/user/update-gcm-id",jo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){}
}