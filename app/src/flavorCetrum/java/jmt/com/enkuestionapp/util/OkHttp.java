package jmt.com.enkuestionapp.util;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;

public class OkHttp {
//    public static String URL="http://192.168.1.103/centrum";
    public static String URL="http://45.55.200.155:9001";
    Context ctx;
    JmStore js;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient client = new OkHttpClient();
    public OkHttp(Context ctx){
        this.ctx = ctx;
        js = new JmStore(ctx);
    }
    public Response makeGetSynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        Response r = client.newCall(request).execute();
        return r;
    }
    private Response makePostRequest(String url, String json) throws IOException {
        RequestBody body2 =  RequestBody.create(JSON, json);
        String gcm_id = js.get(S.GCMID);
        Request.Builder rb= new Request.Builder()
                .url(url)
                .addHeader(S.TOKEN, js.get(S.TOKEN))
                .addHeader(S.Cookie, js.get(S.Cookie))
                .addHeader(S.IMEI, js.get(S.IMEI))
                .addHeader(S.GCMID, gcm_id);
        Request request = prepare_request(url).post(body2).build();
        return client.newCall(request).execute();
    }
    private Request.Builder prepare_request(String url){
        String gcm_id = js.get(S.GCMID);
        System.out.println("---------------------------------------");
        System.out.println(js.get(S.Cookie));
        return new Request.Builder()
            .url(url)
            .addHeader(S.TOKEN, js.get(S.TOKEN))
            .addHeader(S.Cookie, js.get(S.Cookie))
            .addHeader(S.IMEI, js.get(S.IMEI))
            .addHeader(S.GCMID, gcm_id);
    }
    private ResponseE showResponse(Response response) {
        ResponseE ss = new ResponseE();
        ss.setMassege("Éxito");
        try {
            String in = response.body().string();
            ss.setSuccess(response.isSuccessful());
            System.out.println("El in es:");
            System.out.println(in);
            JSONObject jo = new JSONObject(in);
            String code = String.valueOf(response.code());
            ss.setCode(code);
            if(!response.isSuccessful()){
                if(code.equals("500")){
                    ss.setMassege("Error del servidor");
                }else{
                    System.out.println("e.e");
                    System.out.println(jo.toString());
                    ss.setMassege(jo.getString("errors"));
                }
            }else{
                ss.setData(jo.getString("data"));
            }
//            if(code.equals(S.RESPONSE.enc401)){
//                System.out.println("A cerrar sesion....!!!");
//                return ss;
//            }

//            if(jo.isNull("massege"))
//                 ss.setMassege(jo.getString("message"));
//            else ss.setMassege(jo.getString("massege"));
            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++){
                if(responseHeaders.name(i).equals(S.TOKEN)){
                    js.save(S.TOKEN, responseHeaders.value(i));
                }
                if(responseHeaders.name(i).equals(S.Set_Cookie)){
                    js.save(S.Cookie, responseHeaders.value(i));
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            ss.setMassege("Error del servidor, no hay informacion");
            ss.setSuccess(false);return ss;}
        catch (JSONException e) {
            e.printStackTrace();
            ss.setMassege("Error del servidor, intente despues");
            ss.setSuccess(false);return ss;}
        return ss;
    }
    public ResponseE makeGetRequest(String service) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makeGetSynchronous(service);
            return showResponse(r);
        }catch (IOException e){
            e.printStackTrace();
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.enc_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
    public ResponseE makePostRequest(String service,JSONObject params) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makePostRequest(service,params.toString());
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.enc_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
}