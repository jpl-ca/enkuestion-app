package jmt.com.enkuestionapp.adapter;
import  android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import jmt.com.enkuestionapp.R;
import jmt.com.enkuestionapp.interfaz.CbOptionSelected;
import jmt.com.enkuestionapp.interfaz.onLoadImageListener;
import jmt.com.enkuestionapp.model.entity.SurveyE;
import jmt.com.enkuestionapp.util.Util;
/**
 * Created by JMTech-Android on 16/07/2015.
 *
 * REEMPLAZA A SurveyAdapterList
 */
public class SurveysAdapterGrid extends RecyclerView.Adapter<SurveysAdapterGrid.ViewHolder> {
    private ArrayList<SurveyE> ds;
    private Activity ctx;
    String pth_file;
    onLoadImageListener cb_load_image;
    public static CbOptionSelected delegate;
    public SurveysAdapterGrid(Activity _ctx, ArrayList<SurveyE> dataArgs, CbOptionSelected callback){
        ds = dataArgs;
        delegate = callback;
        ctx = _ctx;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridview_survey, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.position = position;
        holder.tv_survey.setText(ds.get(position).getGroup().getCourse_name());
        String url = ds.get(position).getUrl_image();
        Util util = new Util(ctx);
        if(url==null||url.length()<3) url = "";
        else Picasso.with(ctx).load(url).placeholder(R.drawable.encuesta_1).into(holder.iv_image_survey);
///////////////////////////////////////////////
//        else setImage(new onLoadImageListener() {
//            @Override
//            public void load(Bitmap bm) {
//                holder.iv_image_survey.setImageBitmap(bm);
//            }
//        }, ds.get(position).getSurvey_id(), url);
///////////////////////////////////////////////
//        else new DownloadImageTask(ctx, new onLoadImageListener() {
//            @Override
//            public void load(Bitmap bm) {
//                holder.iv_image_survey.setImageBitmap(bm);
//            }
//        }).execute(url,String.valueOf(ds.get(position).getSurvey_id()));

        holder.ll_question.getLayoutParams().height = util.heightFromScreen(4,3);
        holder.iv_image_survey.getLayoutParams().height = util.heightFromScreen(16,9);
    }
    @Override
    public int getItemCount() {
        return ds.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView tv_survey,tv_pts;
        protected LinearLayout ll_question;
        protected int position;
        protected ImageView iv_image_survey;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_survey =  (TextView) itemView.findViewById(R.id.tv_survey);
            iv_image_survey =  (ImageView) itemView.findViewById(R.id.iv_image_survey);
            ll_question =  (LinearLayout) itemView.findViewById(R.id.ll_question);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            delegate.selected(position);
        }
    }



    public void setImage(onLoadImageListener delegate, int id,String url){
        pth_file = "JM/encuesta000"+id+"."+(url.endsWith("png")?"png":"jpg");
        cb_load_image = delegate;
        File file = new File(Environment.getExternalStorageDirectory().getPath() + pth_file);
        if(file.exists()){
            Bitmap bmp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + pth_file);
            System.out.println("Archivo ya existe!!!");
            System.out.println("Archivo ya existe!!!");
            System.out.println("Archivo ya existe!!!");
            delegate.load(bmp);
            return;
        }
        Picasso.with(ctx).load(url).into(target);
    }
    Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            cb_load_image.load(bitmap);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + pth_file);
                    File parent = file.getParentFile();
                    if (parent != null) parent.mkdirs();
                    try {
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,100,ostream);
                        ostream.close();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        @Override
        public void onBitmapFailed(Drawable errorDrawable) {}
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {}
    };
}