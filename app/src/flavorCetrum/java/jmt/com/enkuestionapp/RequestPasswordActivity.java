package jmt.com.enkuestionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.logging.Handler;

import jmt.com.enkuestionapp.interfaz.CallbackRequest;
import jmt.com.enkuestionapp.model.JmStore;
import jmt.com.enkuestionapp.model.S;
import jmt.com.enkuestionapp.model.entity.ResponseE;
import jmt.com.enkuestionapp.model.entity.UserE;
import jmt.com.enkuestionapp.service.LoginTask;
import jmt.com.enkuestionapp.service.RequestPasswordTask;
import jmt.com.enkuestionapp.util.HttpApi;
import jmt.com.enkuestionapp.util.Util;

public class RequestPasswordActivity extends AppCompatActivity{
    private EditText txt_email;
    private RelativeLayout root_layout;
    private Button btnSend;
    HttpApi http;
    UserE user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_password);
        setupComponents();
    }

    private void setupComponents(){
        user = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(new JmStore(this).get(S.USUARIO.USUARIO),UserE.class);
        String email = "";
        http = new HttpApi();
        root_layout = (RelativeLayout) findViewById(R.id.rootLayout);
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_email.setText(email);
        btnSend = (Button) findViewById(R.id.btn_solicitar);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPassword();
            }
        });
    }

    private void requestPassword(){
        String email = txt_email.getText().toString();
        if(TextUtils.isEmpty(email)||!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            new Util(this).showToast(root_layout,getString(R.string.s_el_correo_electronico_necesario));
            return;
        }
        enableControl(false);
        new RequestPasswordTask(this, new CallbackRequest() {
            @Override
            public void processFinish(String msj) {
                new Util(RequestPasswordActivity.this).showToast(root_layout, msj);
                enableControl(true);
            }
            @Override
            public void processFinish(ResponseE response) {
                String msj = "Su solicitud de contraseña fue enviado a su correo electrónico";
                try {
                    JSONObject jo = new JSONObject(response.getData());
                    msj = jo.getString("message");
                    System.out.println(jo.getString("password"));
                }catch (Exception e){}

                new Util(RequestPasswordActivity.this).showToast(root_layout, msj);
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ingresar();
                    }
                }, 2000);
            }
        }).execute(email);
    }

    private void enableControl(boolean b){
        txt_email.setEnabled(b);
        btnSend.setEnabled(b);
    }
    private void ingresar(){
        finish();
    }
}